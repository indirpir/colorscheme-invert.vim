from setuptools import setup

setup(name='colorscheme-invert.vim',
      version='0.1',
      description='Package for inverting vim colorschemes',
      url='http://github.com/yanlobkarev/colorscheme-invert.vim',
      author='indirpir',
      author_email='yan.lobkarev@gmail.com',
      license='MIT',
      packages=['colorscheme_invert'],
      # scripts=['bin/gist-vimrc'],
      entry_points={
        'console_scripts': ['vim-colorscheme-invert = colorscheme_invert:main'
      ]},
      zip_safe=False)
