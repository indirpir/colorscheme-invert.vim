let g:colorscheme_invert_script = expand('<sfile>:p') 


function! s:RunInvertScript(args)
    let com = split(g:colorscheme_invert_script, '/', 1)
    let com = com[:-2] + ['exe.py']
    let com = join(com, '/')
    let com = com.' --vimruntime='.$VIMRUNTIME
    let com = com.' --vim='.$VIM
    if exists('g:plug_home')
        let com = com.' --plug-home='.g:plug_home
    endif

    if has('nvim')
        let com = com.' --nvim'
    endif
    let com = com.' --source-colorscheme='.a:args

    let out = system(com)
    let code = v:shell_error 
    if code != 0
        echon code
        echon "\n"
        echon out
    else
        execute 'colorscheme '.out 
    endif
endfunction


command! -complete=color -nargs=1 ColorschemeInvert call <SID>RunInvertScript('<args>')
