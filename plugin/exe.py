#!/usr/bin/env python
import os
import sys
import glob
import argparse

from invert import invert_colorscheme


RETURN_OK = 0
RETURN_CAUGHT_ERROR = 2


class ColorschemeInvertError(ValueError):
    pass


class ColorschemeInverter(object):
    def __init__(self, scheme, vimruntime='', vim='', plug_home='', is_nvim=False):
        self.scheme = scheme
        self.inverted_scheme = self.get_inverted_scheme_name(scheme)
        self.vimruntime = vimruntime
        self.vim = vim
        self.plug_home = plug_home
        self.is_nvim = is_nvim

    def invert(self):
        if not self.is_scheme_exists(self.inverted_scheme):
            self.invert_scheme(self.scheme, self.inverted_scheme)

        return self.inverted_scheme

    def invert_scheme(self, scheme, inverted_scheme):
        source_scheme = '{}.vim'.format(scheme)
        inverted_scheme = '{}.vim'.format(inverted_scheme)
        source_scheme_directory = self.get_colorschemes_dirs().get(source_scheme)

        if not source_scheme_directory:
            msg = 'Can not locate colorscheme ("{}") source directory'.format(source_scheme)
            raise ColorschemeInvertError(msg)

        source_path = os.path.join(source_scheme_directory, source_scheme)
        inverted_path = os.path.join(source_scheme_directory, inverted_scheme)
        invert_colorscheme(source_path, inverted_path)

    def is_scheme_exists(self, scheme):
        return '{}.vim'.format(scheme) in self.get_colorschemes()

    def get_inverted_scheme_name(self, scheme):
        if scheme.endswith('_inverted'):
            return scheme[: -len('_inverted')]
        return '{}_inverted'.format(scheme)

    def get_colorschemes(self):
        colordirs = self.get_colordirs()
        return [scheme for d in colordirs for scheme in os.listdir(d)]

    def get_colorschemes_dirs(self, inverted=False):
        colordirs = self.get_colordirs()
        return {scheme: d for d in colordirs for scheme in os.listdir(d)}

    def get_colordirs(self):
        # TODO:
        # if self.is_nvim:
        # else:
        #   '~/.vim/colors'
        dot_colors = os.path.expanduser('~/.config/nvim/colors')  # TODO: support win users?
        colordirs = [dot_colors]

        if self.plug_home:
            colordirs += glob.glob('{}/*/colors'.format(self.plug_home))
        if self.vimruntime:
            path = os.path.join(self.vimruntime, 'colors')
            colordirs.append(path)
        elif self.vim:
            path = os.path.join(self.vim, 'runtime', 'colors')
            colordirs.append(path)

        return filter(os.path.exists, colordirs)


def build_inverter():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--vimruntime', type=str, dest='vimruntime',
                        help='Content of $VIMRUNTIME variable')
    parser.add_argument('--vim', type=str, dest='vim',
                        help='Content of $VIM variable')
    parser.add_argument('--plug-home', type=str, dest='plug_home',
                        help='Home of vim Plug')
    parser.add_argument('--nvim', dest='is_nvim', action='store_const',
                        const=True, default=False,
                        help='--nvim flag signalizes to search colors/ dir for nvim')
    parser.add_argument('--source-colorscheme', dest='source_colorscheme',
                        help='colorscheme name to be inverted')

    args = parser.parse_args()
    return ColorschemeInverter(args.source_colorscheme,
                               vimruntime=args.vimruntime,
                               vim=args.vim,
                               plug_home=args.plug_home,
                               is_nvim=args.is_nvim)


if __name__ == '__main__':
    command = build_inverter()
    try:
        inverted = command.invert()
        print(inverted)
        sys.exit(RETURN_OK)
    except ColorschemeInvertError as e:
        print(str(e))
        sys.exit(RETURN_CAUGHT_ERROR)
