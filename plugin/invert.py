#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re
import codecs

from colortrans import short2rgb, rgb2short


def replace_using_regex(text, regexp, prefix, is_hex=False):
    replaced = []
    res = re.split(regexp, text, flags=re.M)

    for s in res:
        if s.startswith(prefix):
            try:
                num = s[len(prefix):]
                num = num.strip()
                if is_hex:
                    inv = int(num, 16) ^ 0xFFFFFF
                    inv = hex(inv)[2:]
                else:
                    # inv = colortrans.short2rgb(num)
                    inv = short2rgb(num)
                    inv = int(inv, 16) ^ 0xFFFFFF
                    inv = hex(inv)[2:]
                    inv = '00000'[:6-len(inv)] + inv
                    # inv, _ = colortrans.rgb2short(inv)
                    inv, _ = rgb2short(inv)
                s = s.replace(num, inv)
            except Exception as e:
                print("Unexpected token: |{}|.".format(s))
                raise e
        replaced.append(s)

    return ''.join(replaced)


def replace_with_inverted(text):
    text = replace_using_regex(text, r"(ctermfg=\d+)\s", "ctermfg=")
    text = replace_using_regex(text, r"(ctermbg=\d+)\s", "ctermbg=")
    text = replace_using_regex(text, r"(guifg=#[\w\d]+)\s", "guifg=#", is_hex=True)
    text = replace_using_regex(text, r"(guibg=#[\w\d]+)\s", "guibg=#", is_hex=True)
    return text


def invert_colorscheme(source_path, inverted_path):
    with codecs.open(source_path, 'r', 'utf-8') as source:
        with codecs.open(inverted_path, 'w', 'utf-8') as inverted:
            out = replace_with_inverted(source.read())
            inverted.write(out)
